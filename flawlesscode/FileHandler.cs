﻿namespace flawlesscode
{
    public class FileHandler
    {
        /*
        * input: folder path and file extension
        * output: all documents in corresponding folder with corresponding file extension
        */
        public static List<string> GetFileContents(string folderPath, string fileExtension)
        {
            List<string> filesContentList = new List<string>();
            foreach (string file in Directory.GetFiles(folderPath, "*." + fileExtension))
            {
                string fileContent = File.ReadAllText(file);
                // Process the file contents
                filesContentList.Add(fileContent);
            }
            return filesContentList;
        }
        /*
        * input: folder path and file extension
        * output: None -> saves the lists in the selected folder
        */
        public static void SaveFiles(string folderPath, List<string> htmlDocumentList)
        {
            for (int i = 0; i < htmlDocumentList.Count; i++)
            {
                string fileName = $"{folderPath}/htmlpage{i + 1}.txt";
                System.IO.File.WriteAllText(fileName, htmlDocumentList[i]);
                Console.WriteLine($"Saved HTML template {i + 1} to {fileName}");

            }
        }
    }
}
