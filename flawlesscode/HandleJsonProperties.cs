﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace flawlesscode
{
    public class HandleJsonProperties
    {
        /*
        * input: attributes object
        * output: html tag with attributes applied
        */
        private static List<string> HandleAttributes(JsonProperty attributes)
        {
            JsonElement attributesValue = attributes.Value;
            List<string> attributeList = new List<string>();

            foreach (JsonProperty attribute in attributesValue.EnumerateObject())
            {
                string attributeName = attribute.Name;
                string attributeValue = attribute.Value.ToString();

                if (attributeName == "style" && attribute.Value.ValueKind == JsonValueKind.Object)
                {
                    attributeList.Add(CreateStyleAttribute(attribute));
                }
                else
                {
                    attributeList.Add($"{attributeName}=\"{attributeValue}\"");
                }
            }
            return attributeList;
        }

        /*
        * input: style object
        * output: html tag with style rules applied
        */
        private static string CreateStyleAttribute(JsonProperty styleObject)
        {
            StringBuilder styleBuilder = new StringBuilder();
            JsonElement styleElement = styleObject.Value;
            string styleElementName = styleObject.Name;
            styleBuilder.Append($"{styleElementName}=\"");

            foreach (JsonProperty styleProperty in styleElement.EnumerateObject())
            {
                string stylePropertyName = styleProperty.Name;
                string stylePropertyValue = styleProperty.Value.ToString();

                string styleRule = $"{stylePropertyName}:{stylePropertyValue};";
                styleBuilder.Append($"{styleRule}");
            }
            styleBuilder.Append("\"");
            string styleAttribute = styleBuilder.ToString();
            return styleAttribute;
        }

        /*
        * input: headtag object
        * output: html list of the headtag element
        */
        public static List<string> GenerateHeaderTags(JsonProperty element)
        {
            List<string> tagList = new List<string>();
            string elementName = element.Name;
            JsonElement elementValue = element.Value;
            foreach (JsonProperty headElement in elementValue.EnumerateObject())
            {
                string headName = headElement.Name;
                if (headName == "meta")
                {
                    tagList.Add($"<{elementName}");
                    foreach (JsonProperty attribute in elementValue.EnumerateObject())
                    {
                        List<string> attributeList = HandleAttributes(attribute);
                        tagList.AddRange(attributeList);
                    }
                    tagList.Add($"/>");
                }
                else if (headName == "link")
                {
                    if (elementValue.ValueKind == JsonValueKind.Array)
                    {
                        foreach (JsonElement arrayElement in elementValue.EnumerateArray())
                        {
                            foreach (JsonProperty attribute in arrayElement.EnumerateObject())
                            {
                                List<string> attributeList = HandleAttributes(attribute);
                                tagList.Add($"<{elementName} {string.Join(" ", attributeList)}/>");
                            }
                        }
                    }
                    else
                    {
                        List<string> attributeList = HandleAttributes(element);
                        tagList.Add($"<{elementName} {string.Join(" ", attributeList)}/>");
                    }
                }
                else
                {
                    tagList.Add($"<{elementName}");
                    foreach (JsonProperty attribute in elementValue.EnumerateObject())
                    {
                        List<string> attributeList = HandleAttributes(attribute);
                        tagList.AddRange(attributeList);
                    }
                    tagList.Add($">");
                }
            }
            return tagList;
        }

        /*
         * input: bodytag object
         * output: html list of the bodytag element
         */
        public static List<string> GenerateBodyTags(JsonProperty element)
        {
            List<string> tagList = new List<string>();
            string elementName = element.Name;
            JsonElement elementValue = element.Value;

            if (elementValue.ValueKind == JsonValueKind.Object)
            {
                tagList.Add($"<{elementName}>");
                foreach (JsonProperty property in elementValue.EnumerateObject())
                {
                    string propertyName = property.Name;
                    JsonElement propertyValue = property.Value;

                    if (propertyName == "attributes" && propertyValue.ValueKind == JsonValueKind.Object)
                    {
                        List<string> attributeList = HandleAttributes(property);
                        tagList.RemoveAt(tagList.Count - 1);
                        tagList.Add($"<{elementName} {string.Join(" ", attributeList)}>");
                    }
                    else if (propertyName == "class" && propertyValue.ValueKind == JsonValueKind.String)
                    {
                        string classValue = propertyValue.GetString();
                        tagList.Add($"<{elementName} class=\"{classValue}\">");
                    }
                    else if (propertyValue.ValueKind == JsonValueKind.Object)
                    {
                        List<string> childTags = GenerateBodyTags(property);
                        tagList.AddRange(childTags);
                    }
                    else
                    {
                        string tag = $"<{propertyName}>{propertyValue}</{propertyName}>";
                        tagList.Add(tag);
                    }
                }

                tagList.Add($"</{elementName}>");
            }
            return tagList;
        }
    }
}
