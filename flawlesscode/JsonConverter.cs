﻿using System.Text.Json;
using System.Text;

namespace flawlesscode;
public class JsonConverter
{
    /*
        * input: stringified json file
        * output: parsed json file
        */
    public static List<JsonDocument> ParseJson(List<string> rawFileContentsList)
    {
        List<JsonDocument> jsonDocumentList = new List<JsonDocument>();
        foreach (string rawFileContents in rawFileContentsList)
        {
            JsonDocument jsonDocument = JsonDocument.Parse(rawFileContents);
            jsonDocumentList.Add(jsonDocument);
        }
        return jsonDocumentList;
    }

    /*
     * input: list with json documents
     * output: list with html templates
    */
    public static List<string> CreateHtmlTemplates(List<JsonDocument> jsonDocumentList)
    {
        List<string> htmlDocumentsList = new List<string>();

        foreach (JsonDocument jsonDocument in jsonDocumentList)
        {
            List<string> htmlTags = new List<string>();
            List<string> htmlBoilerplateList = new List<string>();
            List<string> bodyList = new List<string>();
            List<string> headList = new List<string>();
            string doctype = "html";
            string language = "<html lang=\"en\">\n";
            foreach (JsonProperty property in jsonDocument.RootElement.EnumerateObject())
            {
                htmlBoilerplateList.Clear();
                htmlTags.Clear();

                string propertyName = property.Name;
                htmlTags.Add(propertyName);
                JsonElement propertyValue = property.Value;

                foreach (string tag in htmlTags)
                {
                    switch (propertyName)
                    {
                        case "doctype":
                            doctype = propertyValue.ToString();
                            break;
                        case "language":
                            language = $"<html {propertyName}=\"{propertyValue}\">\n";
                            break;
                        case "head":
                            // This is not working
                            // headList = HandleJsonProperties.GenerateHeadTags(property);
                            break;
                        case "body":
                            bodyList = HandleJsonProperties.GenerateBodyTags(property);
                            break;
                        default:
                            break;
                    }
                }   
            }
            StringBuilder htmlTemplate = new StringBuilder();
            htmlTemplate.Append($"<!DOCTYPE {doctype}>\n");
            htmlTemplate.Append(language);

            foreach (string list in bodyList)
            {
                htmlTemplate.Append(list + "\n");
            }

            htmlTemplate.Append($"</html>\n");
            htmlDocumentsList.Add(htmlTemplate.ToString());
        }
        return htmlDocumentsList;
    }
}
