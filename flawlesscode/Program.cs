﻿// See https://aka.ms/new-console-template for more information
using System.Text.Json;
using flawlesscode;

string folderPath = "C:/flawless";
string fileExtension = "json";
string folderPathSave = "C:/flawless";
Console.WriteLine($"Default values\nFolder path to read from: {folderPath}\nSearching for files with filextension: {fileExtension}\nFolder path to save in: {folderPathSave}\n\nDo you want to use these default values? (yes/no): ");
string response = Console.ReadLine();

if (response.ToLower() != "yes")
{
    Console.WriteLine("Enter the folder path to read from: ");
    folderPath = Console.ReadLine();

    Console.WriteLine("Enter the search pattern: ");
    fileExtension = Console.ReadLine();

    Console.WriteLine("Enter the folder path to save the new files in: ");
    folderPathSave = Console.ReadLine();
}

List<string> rawFileContentsList = FileHandler.GetFileContents(folderPath, fileExtension);
List<JsonDocument> jsonDocumentList = JsonConverter.ParseJson(rawFileContentsList);
List<string> htmlDocumentList = JsonConverter.CreateHtmlTemplates(jsonDocumentList);
FileHandler.SaveFiles(folderPathSave, htmlDocumentList);
